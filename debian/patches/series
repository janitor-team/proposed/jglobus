# DERObjectIdentifier is obsolete
# https://github.com/jglobus/JGlobus/pull/149
jglobus-DERObjectIdentifier-is-obsolete.patch

# Don't force SSLv3 in myproxy, allow TLS
# Backport from git (trunk)
jglobus-dont-force-SSLv3.patch

# Relax proxy validation to be RFC-3820 compliant
# https://github.com/jglobus/JGlobus/issues/160
# https://github.com/jglobus/JGlobus/pull/165
jglobus-key-usage.patch

# Fix javadoc
# https://github.com/jglobus/JGlobus/pull/162
jglobus-javadoc.patch

# Do not accumulate matches in
# GlobusPathMatchingResourcePatternResolver
# https://github.com/jglobus/JGlobus/pull/157
jglobus-do-not-accumulate-matches-in-GlobusPathMatchingResou.patch

# Compatibility with clients that request minimum TLS version 1.2
# https://github.com/jglobus/JGlobus/pull/166
jglobus-do-not-force-SSLv3-TLSv1-allow-TLSv1.1-TLSv1.2.patch

# Remove synchronization on CRL in CRLChecker
# Drop workaround for race condition in BouncyCastle < 1.46
# Reduced lock contention leads to higher request throughput
# Backport from git (trunk and 2.1 branch)
jglobus-remove-synchronization-on-CRL-in-CRLChecker.patch

# Fix "no key" error for PKCS#8 encoded keys
# https://github.com/jglobus/JGlobus/issues/118
# https://github.com/jglobus/JGlobus/issues/146
# https://github.com/jglobus/JGlobus/pull/164
jglobus-support-PKCS8-key-format.patch

# Only allow TLSv1 and TLSv1.2 (not TLSv1.1)
# https://github.com/jglobus/JGlobus/pull/166
jglobus-only-allow-TLSv1-and-TLSv1.2-not-TLSv1.1.patch

# Remove unused FORCE_SSLV3_AND_CONSTRAIN_CIPHERSUITES_FOR_GRAM
# https://github.com/jglobus/JGlobus/pull/166
jglobus-remove-unused-FORCE_SSLV3_AND_CONSTRAIN_CIPHERSUITES.patch

# Adapt to changes in bouncycastle 1.61
# https://github.com/jglobus/JGlobus/pull/168
jglobus-adapt-to-changes-in-PrivateKeyInfo-class.patch
